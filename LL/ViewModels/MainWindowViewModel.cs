﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LL.Models;
using System.Windows.Input;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Shapes;
using LL.Views;
using System.ComponentModel;

using System.Collections.ObjectModel;

using LL.App_Code;

namespace LL.ViewModels
{
    public class MainWindowViewModel : NotifyPropertyChangedImplements
    {
        #region Constructor

        public MainWindowViewModel()
        {
            // Добавить книгу
            AddBookCommand = new Command(arg => {       
            var editBook = new AddBookView();
            var addBookViewModel = new AddBookViewModel();
            editBook.DataContext = addBookViewModel;
            editBook.ShowDialog();
        });
            // Удалить книгу
            DeleteBookCommand = new Command(selectedBook => 
            {
                if (selectedBook == null) return; // TODO: Реализовать отключение команды с помощью CanExecute
                if(!BooksModel.DeleteBook(((Book)selectedBook).Id)) 
                    MessageBox.Show("Error Deleting Books", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            });
            // Добавить заметку
            AddNoteCommand = new Command(selectedBook =>
                {
                    if (selectedBook == null) return; // TODO: Реализовать отключение команды с помощью CanExecute
                    var addNoteWindow = new AddNoteView();
                    var addNoteWiewModel = new AddNoteViewModel();
                    addNoteWiewModel.SelectedBookId = ((Book)selectedBook).Id;
                    addNoteWindow.DataContext = addNoteWiewModel;
                    addNoteWindow.ShowDialog();

                });
            // Удалить заметку
            DeleteNoteCommand = new Command(selectNote =>
            {
                if (selectNote == null) return; // TODO: Реализовать отключение команды с помощью CanExecute
                if (!BooksModel.DeleteNote(((Note)selectNote).Id)) 
                    MessageBox.Show("Error Deleting Notes", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }); 
            ShowNoteInViewerCommand = new Command(arg => ShowNoteInViewerMethod(arg));
            EditingBookCommand = new Command(arg => ChangeEditBookMethod()); // Редактирование книги
            EditingNoteCommand = new Command(arg => ChangeEditNoteMethod());// Редактирование закладки

            ShowApplicationSettingsCommand = new Command(arg => new SettingsView().ShowDialog()); // Показать настройки приложения

            AboutBoxShowCommand = new Command(arg => { new AboutLLView().ShowDialog(); });// Окно о программе
            BooksModel = ((App)(Application.Current)).BooksModel; // Модель данных
            ChoosePathCommand = new Command(arg => ChoosePathMethod(arg)); // Выбор книги на жестком диске
            ChangeSelectedListItemComand = new Command(arg => SelectChanged(arg)); // Изменение выбранной книги
        }
        /// <summary>
        /// Отобразить заметку во вьювере
        /// </summary>
        /// <param name="selectedNote"></param>
        /// <returns></returns>
        private void ShowNoteInViewerMethod(object selectedNote)
        {
            StringBuilder strBld = new StringBuilder();

            strBld.Append("----------TITLE----------");
            strBld.Append(Environment.NewLine);
            strBld.Append(Environment.NewLine);
            strBld.Append(((Note)selectedNote).Title);
            strBld.Append(Environment.NewLine);
            strBld.Append(Environment.NewLine);
            strBld.Append("----------TAG----------");
            strBld.Append(Environment.NewLine);
            strBld.Append(Environment.NewLine);
            strBld.Append(((Note)selectedNote).Tag);
            strBld.Append(Environment.NewLine);
            strBld.Append(Environment.NewLine);
            strBld.Append("----------CODE----------");
            strBld.Append(Environment.NewLine);
            strBld.Append(Environment.NewLine);
            strBld.Append(((Note)selectedNote).Code);
            strBld.Append(Environment.NewLine);
            strBld.Append(Environment.NewLine);
            strBld.Append("----------DESCRIPTION----------");
            strBld.Append(Environment.NewLine);
            strBld.Append(Environment.NewLine);
            strBld.Append(((Note)selectedNote).Description);

            Global.NoteViewerViewModel.Value.ViewerContent = strBld.ToString();
            new NoteViewerView().ShowDialog();
        }       
            
        /// <summary>
        /// Сменить оасположение книги на жестком диске
        /// </summary>
        /// <returns></returns>
        private void ChoosePathMethod(object selectedBook)
        {
            var ofd = new Microsoft.Win32.OpenFileDialog();
            if (ofd.ShowDialog() == true) { BooksModel.Books.SingleOrDefault(book=> book.Id == ((Book)selectedBook).Id).BookLocation = ofd.FileName; }
        }
        /// <summary>
        /// Включение возможности редактирования Заметки
        /// </summary>
        private void ChangeEditNoteMethod()
        {
            if (IsEditingNote == true)
                IsEditingNote = false;
            else
                IsEditingNote = true;
        }
        /// <summary>
        /// Включение возможности редактрования книги
        /// </summary>
        private void ChangeEditBookMethod()
        {
            if (IsEditingBook == true)
                IsEditingBook = false;
            else
                IsEditingBook = true;
        }        

        private List<Note> listSource;
        public List<Note> ListSource
        {
            get { return listSource; }
            set 
            {
                if (ListSource == value) return;
                listSource = value;
                OnNotifyPropertyChanged(this, "ListSource");
                
            }
        }

        #endregion

        private string applicationStatus = "Ready";
        /// <summary>
        /// Состояние приложения
        /// </summary>
        public string ApplicationStatus
        {
            get { return applicationStatus; }
            set 
            {
                if (ApplicationStatus == value) return;
                applicationStatus = value;
                OnNotifyPropertyChanged(this, "ApplicationStatus");                
            }
        }

        private bool isEditingNote = true;
        /// <summary>
        /// Редактирование заметки ON/OFF
        /// </summary>
        public bool IsEditingNote
        {
            get { return isEditingNote; }
            set
            {
                if (IsEditingNote == value) return;
                isEditingNote = value;
                OnNotifyPropertyChanged(this, "IsEditingNote");


                if (value == false)
                    ApplicationStatus = "Note Editing...";
                else ApplicationStatus = "Ready";               
            }
        }
        
        private bool isEditingBook = true;
        /// <summary>
        /// Редактирование книги ON/OFF
        /// </summary>
        public bool IsEditingBook
        {
            get { return isEditingBook; }
            set
            {
                if (IsEditingBook == value) return;
                isEditingBook = value;
                OnNotifyPropertyChanged(this, "IsEditingBook");

                 if (value == false)
                    ApplicationStatus = "Book Editing...";
                else ApplicationStatus = "Ready";

                 if (IsEditingBook == true)
                     IsExpanded = true;
            }
        }

        private bool isExpanded;

        public bool IsExpanded
        {
            //get { return isExpanded; }
            set 
            {
                //if (IsExpanded == value) return;
                isExpanded = value;
                OnNotifyPropertyChanged(this, "IsExpanded");
            }
        }

        public ICommand ShowApplicationSettingsCommand { get; set; }
        
        /// <summary>
        /// Показать заметку во вьювере
        /// </summary>
        public ICommand ShowNoteInViewerCommand { get; set; }
        /// <summary>
        /// Окно о программе
        /// </summary>
        public ICommand AboutBoxShowCommand { get; set; }
        /// <summary>
        /// Удалить заметку
        /// </summary>
        public ICommand DeleteNoteCommand { get; set; }
        /// <summary>
        /// Удаление книги
        /// </summary>
        public ICommand DeleteBookCommand { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ICommand ChoosePathCommand { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ICommand EditingNoteCommand { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ICommand EditingBookCommand { get; set; }        
        /// <summary>
        /// Добавляем Книгу
        /// </summary>
        public ICommand AddBookCommand { get; set; }
        /// <summary>
        /// Добавляес закладку
        /// </summary>
        public ICommand AddNoteCommand { get; set; }
        /// <summary>
        /// Выбор в списке новой книги
        /// </summary>
        public ICommand ChangeSelectedListItemComand { get; set; }       
        /// <summary>
        /// Модель данных
        /// </summary>
        public BooksModel BooksModel { get; set; }     
        

        //public event PropertyChangedEventHandler PropertyChanged;

        //public void OnPropertyChanged(string propertyName)
        //{
        //    if (PropertyChanged != null)
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName: propertyName));
        //}

        public void SelectChanged(object parameter)
        {
            if (parameter != null)
            ListSource = BooksModel.Notes.Where(notes => notes.BookId == ((Book)parameter).Id).ToList();            
        }        
    } 
}
