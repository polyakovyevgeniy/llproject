﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace LL.Models
{
    public class Book: BaseModel
    {
        #region Properties

        private string _title;
        /// <summary>
        /// Возвращает или устанавливает название книги
        /// </summary>
        public string Title
        {
            get { return _title; }
            set 
            {
                if (this.Title == value) return;
                _title = value; 
                base.NotifyPropertyChanged(this, "Title");
            }
        }
        /// <summary>
        /// Возврвщает или устанавливает идентификатор книги
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Возврвщает или устанавливает путь к книге на жестком диске
        /// </summary>
        private string _bookLocation;

        public string BookLocation
        {
            get { return _bookLocation; }
            set
            {              
                if (this.BookLocation == value) return;
                _bookLocation = value;
                base.NotifyPropertyChanged(this, "BookLocation");
            }
        }

        private string _useCode;
        /// <summary>
        /// Возвращает или устанавливает часто используемый код в книге
        /// </summary>
        public string UseCode
        {
            get { return _useCode; }
            set
            {
                _useCode = value;
                if (this.UseCode == value) return;
                _useCode = value;
                base.NotifyPropertyChanged(this, "UseCode");
            }
        }

        #endregion
    }
}