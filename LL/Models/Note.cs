﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace LL.Models
{
    public class Note : BaseModel
    {
        #region Properties

        private long _id;
        /// <summary>
        /// Возвращает или устанавливает идентификатор заметки
        /// </summary>
        public long Id
        {
            get { return _id; }
            set 
            {
                if (this.Id == value) return;
                if (value >= long.MinValue)
                    base.AddError("Id", "Exceeded The Limit Of Notes!!!");
                else
                    base.RemoveError("Id");

                this._id = value;
                base.NotifyPropertyChanged(this,"Id");
            }
        }
        private long _bookId;
        /// <summary>
        /// Возвращает или устанавливает идентификатор книги, которой принадлежит заметка
        /// </summary>
        public long BookId
        {
            get { return _bookId; }
            set 
            {
                if (this.BookId == value) return;
                if (value >= long.MinValue)
                    base.AddError("BookId", "Exceeded The Limit Of Books!!!");
                else
                    base.RemoveError("BookId");

                this._bookId = value;
                base.NotifyPropertyChanged(this, "BookId");
            }
        }
        private string _title;
        /// <summary>
        /// Устанавливает или возврвщает название заметки
        /// </summary>
        public string Title
        {
            get { return _title; }
            set 
            {
                if (this.Title == value) return;
                _title = value; 
                base.NotifyPropertyChanged(this,"Title");
            }
        }

        private string _tag;
        /// <summary>
        /// Возврвщает или устанавливает тег заметки
        /// </summary>
        public string Tag
        {
            get { return _tag; }
            set
            {
                if(this.Tag == value) return;
                _tag = value;
                base.NotifyPropertyChanged(this, "Tag");
            }
        }

        private string _description;
        /// <summary>
        /// Возврвщает или устанавливает описание заметки
        /// </summary>
        public string Description
        {
            get { return _description; }
            set
            {
                if(this.Description == value) return;
                _description = value;
                base.NotifyPropertyChanged(this, "Description");
            }
        }

        private string _code;
        /// <summary>
        /// Возвращает или устанавливает код заметки
        /// </summary>
        public string Code
        {
            get { return _code; }
            set
            {
                if(this.Code == value) return;
                _code = value;
                base.NotifyPropertyChanged(this, "Code");
            }
        }
       
        /// <summary>
        /// Возвращает или устанавливает дату создания заметки
        /// </summary>
        public string CreationDate { get; set; }

        private int _pageNumber;
        /// <summary>
        /// Возващает или устанавливает страницу в книге относительно которой была добавлена заметка
        /// </summary>
        public int PageNumber
        {
            get { return _pageNumber; }
            set
            {
                if(this.PageNumber == value) return;
                _pageNumber = value;
                base.NotifyPropertyChanged(this, "PageNumber");
            }
        }

        #endregion       
    }
}