﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using LL.Models;

namespace LL
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        ///  Модель данных
        /// </summary>
        public BooksModel BooksModel { get; set; }
        private void Application_Startup(object sender, StartupEventArgs e)
        {
             BooksModel = new BooksModel();
        }
    }
}
